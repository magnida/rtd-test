from setuptools import setup, find_packages


setup(
    name='magnida-rtd-test',
    version="1.0",
    license="MIT",
    description='test',
    author='Daniel Magni',
    author_email='eric@ericholscher.com',
    url='http://readthedocs.org',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Framework :: Django',
    ],
)
